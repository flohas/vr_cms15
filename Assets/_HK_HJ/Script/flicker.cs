﻿using UnityEngine;
using System.Collections;

public class flicker : MonoBehaviour {
    //speed light should flicker
	float flickerSpeed = 4.75f;

    public AudioSource fireplaceAudio;

    // Use this for initialization
    void Start() {
        fireplaceAudio.Stop(); //disable light and sound
        GetComponent <Light>(). intensity = 0;
        //start coroutine to flicker light
		StartCoroutine("FlickerLight");
	}

	IEnumerator FlickerLight() {
        //wait for 2 seconds before flickering starts
		yield return new WaitForSeconds (2);
        //start sound
        fireplaceAudio.Play();
        while (true) {
            //start flickering
		    GetComponent<Light> ().intensity = Random.Range (3, flickerSpeed);
		    //print("FlickerLight Loop");
			yield return new WaitForSeconds (Random.Range(0,1));
		}
	}


}

