﻿using UnityEngine;
using System.Collections;

public class MuralArtController : MonoBehaviour {
	
	public float timeLeftFirst = 5.0f;
	private float startTime;

	bool isFirst = false;

	void OnEnable(){
		GameLogic.OnDarkSideChanged += StartTimerForDarkSide;
		GameLogic.OnGoodSideChanged += SwitchToTheGoodSide;
	}

	void onDisable(){

		GameLogic.OnDarkSideChanged -= StartTimerForDarkSide;
		GameLogic.OnGoodSideChanged -= SwitchToTheGoodSide;
	}

	// Use this for initialization
	void Start () {
		SwitchToTheGoodSide ();
	}

	void Update()
	{
		if (isFirst) {
			if (Time.time >= timeLeftFirst) {
				SwitchToTheDarkSide ();
			}
		}
	}


	void SwitchToTheGoodSide(){
		
		Transform[] children = gameObject.GetComponentsInChildren<Transform> ();

		for (int i = 0; i < children.Length; i++) {
			MeshRenderer mr = children [i].GetComponent<MeshRenderer> ();
			if (children [i].name.Contains ("_good")) {
				mr.enabled = true;

			} else if (children [i].name.Contains ("_bad")) {
				mr.enabled = false;
			}
		}
	}

	void StartTimerForDarkSide(){
		
		timeLeftFirst += GameLogic.startTime;
		isFirst = true;
	}

	void SwitchToTheDarkSide(){
		Transform[] children = gameObject.GetComponentsInChildren<Transform> ();

		for (int i = 0; i < children.Length; i++) {
			MeshRenderer mr = children [i].GetComponent<MeshRenderer> ();
			if (children [i].name.Contains ("_good")) {
				mr.enabled = false;

			} else if (children [i].name.Contains ("_bad")) {
				mr.enabled = true;
			}
		}

		isFirst = false;
	}
}

