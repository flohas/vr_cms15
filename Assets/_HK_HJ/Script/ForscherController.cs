﻿using UnityEngine;
using System.Collections;

public class ForscherController : MonoBehaviour {

	bool isActive=true;

	static float sRATE = 40f;

	float timeToGo;
	float startTime;

	void OnEnable(){
		GameLogic.OnDarkSideChanged += enableForscher;
		GameLogic.OnGoodSideChanged += disableForscher;
	}

	void onDisable(){
		GameLogic.OnDarkSideChanged -= enableForscher;
		GameLogic.OnGoodSideChanged -= disableForscher;
	}

	void Start(){
		disableForscher ();
	}

	void FixedUpdate(){
		if (isActive) {
			if (Time.time >= timeToGo) {
				MeshRenderer[] forscheMesh = gameObject.GetComponentsInChildren<MeshRenderer> ();
				for (int i = 0; i < forscheMesh.Length; i++) {
					forscheMesh [i].enabled = true;
				}
				isActive = false;
			}
		}
	}

	void enableForscher(){
		gameObject.SetActive (true);
		MeshRenderer[] forscheMesh = gameObject.GetComponentsInChildren<MeshRenderer> ();
		for (int i = 0; i < forscheMesh.Length; i++) {
			forscheMesh [i].enabled = false;
		}
	}

	void disableForscher(){
		startTime = Time.time;
		timeToGo = startTime + sRATE;
		gameObject.SetActive (false);
	}

}
