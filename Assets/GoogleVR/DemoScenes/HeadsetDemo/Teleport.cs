using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Collider))]
public class Teleport : MonoBehaviour, IGvrGazeResponder {
  private Vector3 startingPosition;
	//private bool hatHingesehen=false; //hilfsvariable



//----------DEFINING-VARIABLES--------------------------------------------------------------------------------
	static int counter=0; //has to be static to remain constant while changing scenes. Otherwise the counter would always restart at '0'. 

	public GameObject window1;
	public GameObject window2;
	public GameObject glass;
	public GameObject petri;
	public Canvas superhotCanvas;

	private Rigidbody rb_glass;
	private Rigidbody rb_petri;

	public Text superhotText;

	public string[] textParts;

	public float duration;
	public float originalIntensity;
	private Light lt; //light component on microscope

	public bool glow=true;

	public float smooth;

	public WindZone windzone_laboratory;
	public GameObject killer_bacteria;


//----------STANDARD-FUNCTIONS-------------------------------------------------------------------------------

void Start() {
    startingPosition = transform.localPosition;
    	textParts = new string[4];
		textParts [0] = "This";
		textParts [1] = "is";
		textParts [2] = "a";
		textParts [3] = "test";

		lt = GetComponent<Light>();

		ParticleSystem ps=killer_bacteria.GetComponent<ParticleSystem>();
		var em = ps.emission;
		em.enabled = false; 

		//Debug.Log ("Startcounter:");
		//Debug.Log (counter);

		//originalIntensity = lt.intensity;

		if (counter == 1) { //folgende Aktionen passieren, sobald der User das erste mal hinsieht
			StartCoroutine("openWindows",2f);//first argument is the name of the desired coroutine, the second argument is in this case the amount of seconds
		} //end of if
		else if (counter == 2) {
			StartCoroutine ("tiltCan");
		} //end of else if 1
		else if (counter == 3) {
			StartCoroutine ("killEveryone");
		}//end of else if 2

	}//end of Start

void Update(){
		if(glow==true){
			StartCoroutine ("ObjectGlowCoroutine");
		}//end of if
		else{
			StopCoroutine ("ObjectGlowCoroutine");
			//langsames Abdunkeln des Glow-Effekts
			lt.intensity = Mathf.Lerp (lt.intensity,0,Time.deltaTime*smooth); 
		}//end of else
	}//end of Update

void LateUpdate() {
		GvrViewer.Instance.UpdateState();
		if (GvrViewer.Instance.BackButtonPressed) {
			Application.Quit();
		}//end of if
	}//end of LateUpdate


//----------OWN-FUNCTIONS-------------------------------------------------------------------------------------------------

public void ObjectGlow(){
		glow = true;
	}//end of ObjectGlow

public void ObjectNotGlow(){
		glow = false;
	}//end of ObjectNotGlow		

 
//----------COROUTINES----------------------------------------------------------------------------------------------------

IEnumerator JourneyToDonut (float waitTime){
	//Enter donut-world
		float fadeTime=GameObject.Find("GameObject_for_fading").GetComponent<fading>().BeginFade(1);	//1, because we want it to fade out
		yield return new WaitForSeconds (fadeTime);
		SceneManager.LoadSceneAsync ("giant_donut", LoadSceneMode.Single); //use 'LoadSceneAsync' to get better performance than 'LoadScene' only. 'Single' means that only this scene will be visible. When using 'Additive', both scenes will be seen. 
		yield return new WaitForSeconds(waitTime); //stay in donut-world for some time
	} //end of 'JourneyToDonut'

IEnumerator openWindows(float waitTime){	//open windows
		window1.transform.Rotate(0,90,0); 
		window1.GetComponent<AudioSource>().Play();
		yield return new WaitForSeconds(waitTime);
		window2.transform.Rotate(0,90,0); 
		window2.GetComponent<AudioSource>().Play();
		windzone_laboratory.windMain=0.5f;	//increase wind strength
	} //end of openWindow
		


IEnumerator tiltCan (){
		rb_glass = glass.GetComponent<Rigidbody>();
		rb_glass.AddForce(-1,1,1);
		windzone_laboratory.windMain=1.0f;	//maximize wind strength
		return null; //scheint man zu brauchen...
	}//end of destroyGlass
		


IEnumerator killEveryone(){
		//first the glasses should be moved again by the wind
		rb_glass = glass.GetComponent<Rigidbody>();
		rb_glass.AddForce(-50,50,50);

		//then the petri glass at the own table gets blown away
		rb_petri = petri.GetComponent<Rigidbody>();
		rb_petri.AddForce(-10,10,10);

		ParticleSystem ps=killer_bacteria.GetComponent<ParticleSystem>();
		var em = ps.emission;
		em.enabled = true; 

		return null; //scheint man zu brauchen...
	}//end of releaseBacteria

IEnumerator showInstruction (float waitTime2){
	//set text active
	for(int i = 0; i < 4; i++) //maybe I can figure out how to insert the amount of textParts here. I got an error because they are string arrays yet...
		{
			superhotCanvas.GetComponent<AudioSource>().Play();
			superhotText.text=textParts[i];
			yield return new WaitForSeconds(waitTime2);
		}//end of for-loop
	}//end of showInstruction
		
IEnumerator ObjectGlowCoroutine (){
		lt.intensity = Mathf.Lerp (lt.intensity,originalIntensity,Time.deltaTime/smooth);
		yield return new WaitForSeconds(smooth);
		float amplitude = Mathf.PingPong(Time.time, duration);
		//Debug.Log (amplitude);
		amplitude = amplitude / duration * 0.5F + 0.5F;
		lt.intensity = originalIntensity * amplitude;
	}//end of ObjectGlowCoroutine

//----------GOOGLE-VR-FUNCTIONS--------------------------------------------------------------------------------------------


public void SetGazedAt(bool gazedAt) { //"gazedAt" ist genau die funktion beim hinsehen
    GetComponent<Renderer>().material.color = gazedAt ? Color.green : Color.red;
		if (gazedAt){
			
			counter=counter+1; //raise counter to  +1
			//Debug.Log (counter);
			if (counter == 1) { //folgende Aktionen passieren, sobald der User das erste mal hinsieht
				StartCoroutine("JourneyToDonut",2f);//first argument is the name of the desired coroutine, the second argument is in this case the amount of seconds
				//StartCoroutine("showInstruction",1f);
			} //end of if
			else if (counter == 2) {
				StartCoroutine ("JourneyToDonut",2f);
			} //end of else if 1
			else if (counter == 3) {
				StartCoroutine ("JourneyToDonut",2f);
			}//end of else if 2

		}//end of if(gazedAt)
	
	}//end of SetGazedAt

public void Reset() {
    //transform.localPosition = startingPosition;
	}//end of Reset

public void ToggleVRMode() {
    GvrViewer.Instance.VRModeEnabled = !GvrViewer.Instance.VRModeEnabled;
	}//end of ToggleVRMode

public void ToggleDistortionCorrection() {
    GvrViewer.Instance.DistortionCorrectionEnabled =
      !GvrViewer.Instance.DistortionCorrectionEnabled;
	}//end of ToggleDistortionCorrection

#if !UNITY_HAS_GOOGLEVR || UNITY_EDITOR
  public void ToggleDirectRender() {
    GvrViewer.Controller.directRender = !GvrViewer.Controller.directRender;
	}//end of ToggleDirectRender
#endif  //  !UNITY_HAS_GOOGLEVR || UNITY_EDITOR


  public void TeleportRandomly() {
	//hier sollte nichts passieren, wir wahren nur die Struktur
	}//end of TeleportRandomly
  

  #region IGvrGazeResponder implementation

	//hier sollte nichts passieren, wir wahren nur die Struktur

  /// Called when the user is looking on a GameObject with this script,
  /// as long as it is set to an appropriate layer (see GvrGaze).
  public void OnGazeEnter() {
    //SetGazedAt(true);
	

  }

  /// Called when the user stops looking on the GameObject, after OnGazeEnter
  /// was already called.
  public void OnGazeExit() {
    //SetGazedAt(false);
	//glow=true;
  }




  /// Called when the viewer's trigger is used, between OnGazeEnter and OnGazeExit.
  public void OnGazeTrigger() {
	//TeleportRandomly();
  }


  #endregion
}
