﻿using UnityEngine;
using UnityEditor;
using System.Collections;

//ATTENTION: Some updates to the tutorial:
//Change "Cardbaord" to "GvrViewer"
//Chanbe "CardboardHead" to "GvrHead"

public class VRBuildSettings : MonoBehaviour {

	[MenuItem("VR Build Settings/Build GearVR")] //Build-settings for GearVR
	static void BuildGearVRSettings(){
		
		PlayerSettings.virtualRealitySupported = true;
		GvrViewer cardboard = (GvrViewer)GameObject.FindGameObjectWithTag ("Player").GetComponent<GvrViewer> ();
		GvrHead cardboardHead = cardboard.gameObject.transform.GetChild (0).gameObject.GetComponent<GvrHead> ();
		cardboard.VRModeEnabled = false; //activates full screen instead of splitting the screen up
		cardboardHead.trackPosition = false; 
		cardboardHead.trackRotation = false;
	


		EditorUtility.SetDirty (cardboard); // if we don't do this, the options won't update on the cardboard
		EditorUtility.SetDirty (cardboardHead);
	}

	[MenuItem("VR Build Settings/Build Cardboard")] //Build-settings for Cardboard
	static void BuildCardboardSettings(){

		PlayerSettings.virtualRealitySupported = false;
		GvrViewer cardboard = (GvrViewer)GameObject.FindGameObjectWithTag ("Player").GetComponent<GvrViewer> ();
		GvrHead cardboardHead = cardboard.gameObject.transform.GetChild (0).gameObject.GetComponent<GvrHead> ();
		cardboard.VRModeEnabled = true; //activates full screen instead of splitting the screen up
		cardboardHead.trackPosition = true; 
		cardboardHead.trackRotation = true;
		EditorUtility.SetDirty (cardboard); // if we don't do this, the options won't update on the cardboard
		EditorUtility.SetDirty (cardboardHead);
	}
}
